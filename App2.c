#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {

    int rank;
    int p;
    int size = 10000;
    int left;
    int right;
  

    MPI_Init(&argc, &argv);

    // rank and size
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &p);



    // Get the name
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //Initalize right and left
    left = (rank - 1 + p) % p;
    right = (rank + 1) % p;
    printf(" I am %d , my left is %d, my right is %d \n", rank, left, right);


    char* processor_name = "";
    if (rank == 0) {
        processor_name = "A";
        printf(" My name is %s. \n", processor_name);
    }
    if (rank == 1) {
        processor_name = "B";
        printf(" My name is %s. \n", processor_name);
    }
    if (rank == 2) {
        processor_name = "C";
        printf(" My name is %s. \n", processor_name);
    }
    if (rank == 3) {
        processor_name = "D";
        printf(" My name is %s. \n", processor_name);
    }
    if (rank == 4) {
        processor_name = "E";
        printf(" My name is %s. \n", processor_name);
    }
    if (rank == 5) {
        processor_name = "F";
        printf(" My name is %s. \n", processor_name);
    }
    if (rank == 6) {
        processor_name = "G";
        printf(" My name is %s. \n", processor_name);
    }
    if (rank == 7) {
        processor_name = "H";
        printf(" My name is %s. \n", processor_name);
    }
    printf("============================================= \n");
    printf("= %s's Sending and Rechieving OPERATIONS = \n", processor_name);
    printf("============================================= \n");
    //Sending name to rank -1 and rank +1.
    MPI_Status  status;
    char rechv_processor_name[10000];

    if (rank == 0) {
        MPI_Send(processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD);
        printf("XXXXXXXX My name is %s                                        XXXX DEBUG  XXXX      XXXX SUCSESSFULLY TRANSMITS THE NAME XXXX\n", processor_name);
        MPI_Send(processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD);
    }
    else if (rank == 1) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD, &status);
        printf("XXXXXXXX My name is %s and i rechieved name of rank 0, it is %s.     XXXX DEBUG  XXXX      XXXX SUCSESSFULLY TRANSMITS THE NAME XXXX\n", processor_name, rechv_processor_name);
        MPI_Send(processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD);

    }
    else if (rank == 7) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD, &status);
        printf("XXXXXXXX My name is %s and i rechieved name of rank 0, it is %s.     XXXX DEBUG  XXXX      XXXX SUCSESSFULLY TRANSMITS THE NAME XXXX\n", processor_name, rechv_processor_name);
        MPI_Send(processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD);
    }
    if (rank == 0) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD, &status);
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD, &status);
    }
    if (rank == 1) {
        MPI_Send(processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD);
    }
    else if (rank == 2) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD, &status);
        printf("XXXXXXXX My name is %s and i rechieved name of rank 1, it is %s.     XXXX DEBUG  XXXX      XXXX SUCSESSFULLY TRANSMITS THE NAME XXXX\n", processor_name, rechv_processor_name);

        MPI_Send(processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD);
    }
    if (rank == 1) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD, &status);
    }


    if (rank == 2) {
        MPI_Send(processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD);
    }
    else if (rank == 3) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD, &status);
        printf("XXXXXXXX My name is %s and i rechieved name of rank 2, it is %s.     XXXX DEBUG XXXX      XXXX SUCSESSFULLY TRANSMITS THE NAME XXXX\n", processor_name, rechv_processor_name);
        MPI_Send(processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD);
    }
    if (rank == 2) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD, &status);
    }

    if (rank == 3) {
        MPI_Send(processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD);
    }
    else if (rank == 4) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD, &status);
        printf("XXXXXXXX My name is %s and i rechieved name of rank 3, it is %s.     XXXX DEBUG  XXXX      XXXX SUCSESSFULLY TRANSMITS THE NAME XXXX\n", processor_name, rechv_processor_name);

        MPI_Send(processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD);
    }
    if (rank == 3) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD, &status);
    }

    if (rank == 4) {
        MPI_Send(processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD);
    }
    else if (rank == 5) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD, &status);
        MPI_Send(processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD);
    }
    if (rank == 4) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD, &status);
    }

    if (rank == 5) {
        MPI_Send(processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD);
    }
    else if (rank == 6) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD, &status);
        MPI_Send(processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD);
    }
    if (rank == 5) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD, &status);
    }

    if (rank == 6) {
        MPI_Send(processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD);
    }
    else if (rank == 7) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD, &status);
        MPI_Send(processor_name, size, MPI_CHAR, left, 0, MPI_COMM_WORLD);
    }
    if (rank == 6) {
        MPI_Recv(rechv_processor_name, size, MPI_CHAR, right, 0, MPI_COMM_WORLD, &status);
    }
    




    printf("============================================= \n");


    // run command for 8 times -> mpiexec -np 8 App2.exe

    return MPI_Finalize();
}
